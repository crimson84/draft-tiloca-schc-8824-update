# Clarifications and Updates on using Static Context Header Compression (SCHC) for the Constrained Application Protocol (CoAP)

This is the working area for the individual Internet-Draft, "Clarifications and Updates on using Static Context Header Compression (SCHC) for the Constrained Application Protocol (CoAP)".

* [Editor's Copy](https://crimson84.gitlab.io/draft-tiloca-schc-8824-update/)

<!--
* [Datatracker Page](https://datatracker.ietf.org/doc/draft-tiloca-schc-8824-update)
* [Individual Draft](https://datatracker.ietf.org/doc/html/draft-tiloca-schc-8824-update)
* [Compare Editor's Copy to Individual Draft](https://git@gitlab.com:crimson84.github.io/draft-tiloca-schc-8824-update/#go.draft-tiloca-schc-8824-update.diff)
-->

## Contributing

See the
[guidelines for contributions](https://gitlab.com/crimson84/draft-tiloca-schc-8824-update/-/blob/main/CONTRIBUTING.md).

Contributions can be made by creating pull requests.
The GitHub interface supports creating pull requests using the Edit (✏) button.


## Command Line Usage

Formatted text and HTML versions of the draft can be built using `make`.

```sh
$ make
```

Command line usage requires that you have the necessary software installed.  See
[the instructions](https://github.com/martinthomson/i-d-template/blob/main/doc/SETUP.md).

